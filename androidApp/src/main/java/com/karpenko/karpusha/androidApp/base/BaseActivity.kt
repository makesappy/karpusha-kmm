package com.karpenko.karpusha.androidApp.base

import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity(){
    protected fun openFragment(){}
}