package com.karpenko.karpusha.androidApp.feature.events

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.karpenko.karpusha.shared.domain.model.Event
import com.karpenko.karpusha.shared.domain.model.Result
import com.karpenko.karpusha.shared.domain.usecase.ObserveEventsUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class EventsViewModel(
    private val observeEventsUseCase: ObserveEventsUseCase
) : ViewModel() {
    private val _events = MutableStateFlow<Result<List<Event>>>(Result.Running())
    val events: StateFlow<Result<List<Event>>> = _events

    init {
        viewModelScope.launch {
            // FIXME now the boolean parameter is dummy, on AN impl it is not used
            observeEventsUseCase(true).watch {
                _events.value = it
            }
        }
    }
}