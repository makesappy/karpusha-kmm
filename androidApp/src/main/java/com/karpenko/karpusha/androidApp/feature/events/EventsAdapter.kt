package com.karpenko.karpusha.androidApp.feature.events

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.karpenko.karpusha.androidApp.R
import com.karpenko.karpusha.androidApp.databinding.ItemEventBinding
import com.karpenko.karpusha.shared.domain.model.Event
import java.util.*

class EventsAdapter(var events: List<Event>) :
    RecyclerView.Adapter<EventsAdapter.EventViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        return LayoutInflater.from(parent.context)
            .inflate(R.layout.item_event, parent, false)
            .run(::EventViewHolder)
    }

    override fun getItemCount(): Int = events.count()

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bindData(events[position])
    }

    inner class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var binding: ItemEventBinding = ItemEventBinding.bind(itemView)

        fun bindData(event: Event) {
            with(binding) {
                when (event) {
                    is Event.Single -> {
                        tvHomeTeam.text = event.homeTeam
                        tvAwayTeam.text = event.awayTeam
                        tvCurrentScore.text = event.currentScore
                        tvAllRecentScores.text = event.allRecentScores
                    }
                    is Event.Multiple -> {
                        groupSingleEvent.isVisible = false
                    }
                }
                tvType.text =
                    event.type.name.toLowerCase(Locale.getDefault()).capitalize(Locale.getDefault())
                tvDescription.text = event.description
            }
        }
    }
}