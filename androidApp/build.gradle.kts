plugins {
    id("com.android.application")
    kotlin("android")
}
group = Sdk.sdkGroup
version = Sdk.sdkVersion

repositories {
    gradlePluginPortal()
    google()
    jcenter()
    mavenCentral()
    maven(URL.snapshots)
    maven(URL.koinEkito)
}
android {
    compileSdkVersion(30)
    defaultConfig {
        applicationId = "com.karpenko.karpusha.androidApp"
        minSdkVersion(24)
        targetSdkVersion(30)
        versionCode = 1
        versionName = "1.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
    buildFeatures {
        viewBinding = true
    }
}
dependencies {
    implementation(project(":shared"))
    implementation(Deps.material)
    implementation(Deps.Android.appCompat)
    implementation(Deps.Android.constraint)
    implementation(Deps.Coroutines.android)
    implementation(Deps.Android.coreKtx)
    implementation(Deps.Android.recycler)
    implementation(Deps.Android.cardView)
    implementation(Deps.Android.viewModelKtx)
}