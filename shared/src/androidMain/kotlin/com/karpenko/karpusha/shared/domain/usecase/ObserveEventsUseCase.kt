package com.karpenko.karpusha.shared.domain.usecase

import com.karpenko.karpusha.shared.data.repository.ConnectivityRepository
import com.karpenko.karpusha.shared.domain.model.CloseableCommonFlow
import com.karpenko.karpusha.shared.domain.model.Event
import com.karpenko.karpusha.shared.domain.model.Result
import com.karpenko.karpusha.shared.domain.model.UseCase
import com.karpenko.karpusha.shared.domain.repository.EventsRepository

actual class ObserveEventsUseCase(
    private val repository: EventsRepository,
    private val connectivity: ConnectivityRepository
) : UseCase<CloseableCommonFlow<Result<List<Event>>>, Boolean>() {
    override suspend fun doWork(params: Boolean) =
        repository.getEvents(connectivity.isConnected())
}