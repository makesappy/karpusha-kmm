package com.karpenko.karpusha.shared.di

import com.karpenko.karpusha.shared.cache.AppDatabase
import com.karpenko.karpusha.shared.data.repository.ConnectivityRepository
import com.karpenko.karpusha.shared.domain.usecase.ObserveEventsUseCase
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver
import org.koin.dsl.module

actual val platformModule = module {
    single<SqlDriver> { AndroidSqliteDriver(AppDatabase.Schema, get(), "test.db") }

    single { ConnectivityRepository(get()) }

    single { ObserveEventsUseCase(get(), get()) }
}