package com.karpenko.karpusha.shared.data.repository

import android.net.ConnectivityManager
import android.content.Context

actual class ConnectivityRepository(
    private val ctx: Context
){
    actual fun isConnected(): Boolean{
        val cm = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        return cm.activeNetworkInfo?.isConnected ?: false
    }
}