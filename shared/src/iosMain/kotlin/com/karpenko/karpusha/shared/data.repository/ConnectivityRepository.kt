package com.karpenko.karpusha.shared.data.repository

import kotlinx.cinterop.cValue
import kotlinx.cinterop.staticCFunction
import platform.SystemConfiguration.SCNetworkReachabilityContext
import platform.SystemConfiguration.SCNetworkReachabilityCreateWithName
import platform.SystemConfiguration.SCNetworkReachabilitySetCallback

actual class ConnectivityRepository {
    actual fun isConnected(): Boolean {
        val target = SCNetworkReachabilityCreateWithName(
            allocator = null,
            nodename = "www.google.com"
        ) ?: return false
        val context = cValue<SCNetworkReachabilityContext> {
            version = 0
            println(
                "ConnectivityRepository iOS, \n" +
                        "            context.version: $version" +
                        "            context.copyDescription: $copyDescription" +
                        "            context.info: $info" +
                        "            context.release: $release" +
                        "            context.retain: $retain"
            )
        }
        return SCNetworkReachabilitySetCallback(
            target = target,
            callout = staticCFunction { reachability, flags, info ->
                println(
                    "ConnectivityRepository iOS, \n" +
                            "           reachability: ${reachability?.rawValue} \n" +
                            "           flags: $flags \n" +
                            "           info: $info"
                )
                if (info == null) return@staticCFunction
            },
            context = context
        )
    }
}