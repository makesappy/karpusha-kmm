package com.karpenko.karpusha.shared.di

import com.karpenko.karpusha.shared.cache.AppDatabase
import com.karpenko.karpusha.shared.data.repository.ConnectivityRepository
import com.karpenko.karpusha.shared.domain.usecase.ObserveEventsUseCase
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.drivers.native.NativeSqliteDriver
import org.koin.dsl.module

actual val platformModule = module {
    single<SqlDriver> { NativeSqliteDriver(AppDatabase.Schema, "events.db") }

    single { ConnectivityRepository() }

    single { ObserveEventsUseCase(get()) }
}