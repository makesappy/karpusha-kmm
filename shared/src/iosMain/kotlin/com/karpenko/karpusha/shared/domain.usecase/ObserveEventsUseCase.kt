package com.karpenko.karpusha.shared.domain.usecase

import com.karpenko.karpusha.shared.domain.model.CloseableCommonFlow
import com.karpenko.karpusha.shared.domain.model.Event
import com.karpenko.karpusha.shared.domain.model.Result
import com.karpenko.karpusha.shared.domain.model.UseCase
import com.karpenko.karpusha.shared.domain.repository.EventsRepository

actual class ObserveEventsUseCase(
    private val repository: EventsRepository
) : UseCase<CloseableCommonFlow<Result<List<Event>>>, IosIsConnected>() {
    override suspend fun doWork(params: IosIsConnected): CloseableCommonFlow<Result<List<Event>>> =
        repository.getEvents(params)
}