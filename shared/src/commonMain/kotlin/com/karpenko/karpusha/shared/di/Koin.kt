package com.karpenko.karpusha.shared.di

import com.karpenko.karpusha.shared.data.repository.EventsRepositoryImpl
import com.karpenko.karpusha.shared.domain.repository.EventsRepository
import com.karpenko.karpusha.shared.infrastructure.db.Database
import com.karpenko.karpusha.shared.infrastructure.network.EventsApi
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.dsl.module

fun initKoin(module: Module) =
    startKoin {
        modules(
            coreModule,
            platformModule
        )
    }.also {
        val koin = it.koin
        val doOnStartup =
            koin.get<() -> Unit>() // doOnStartup is a lambda which is implemented in Swift on iOS side
        doOnStartup.invoke()
    }

private val coreModule = module {
    single { EventsApi() }
    single { Database(get()) }
    single<EventsRepository> { EventsRepositoryImpl(get(), get()) }
}

expect val platformModule: Module