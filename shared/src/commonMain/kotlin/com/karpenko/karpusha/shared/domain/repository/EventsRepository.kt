package com.karpenko.karpusha.shared.domain.repository

import com.karpenko.karpusha.shared.domain.model.CloseableCommonFlow
import com.karpenko.karpusha.shared.domain.model.Event
import com.karpenko.karpusha.shared.domain.model.Result

interface EventsRepository {
    suspend fun getEvents(isConnected: Boolean): CloseableCommonFlow<Result<List<Event>>>
}