package com.karpenko.karpusha.shared.infrastructure.db

import com.karpenko.karpusha.shared.cache.AppDatabase
import com.karpenko.karpusha.shared.domain.model.Event
import com.karpenko.karpusha.shared.domain.model.Type
import com.squareup.sqldelight.db.SqlDriver

@Suppress("DeprecatedCallableAddReplaceWith")
class Database(driver: SqlDriver) {
    private val database = AppDatabase(driver)
    private val dbQuery = database.appDatabaseQueries

    internal fun clearDatabase() {
        dbQuery.transaction {
            dbQuery.removeAllEvents()
        }
    }

    private fun insertEvent(event: Event) =
        when (event) {
            is Event.Single -> dbQuery.insertSingleEvent(
                event.type.name,
                event.homeTeam,
                event.awayTeam,
                event.currentScore,
                event.allRecentScores,
                event.description
            )
            is Event.Multiple -> dbQuery.insertMultipleEvent(
                type = event.type.name,
                description = event.description
            )
        }

    internal fun getAllEvents(): List<Event> {
        return dbQuery.selectAllEvents(::mapEvent).executeAsList()
    }

    private fun mapEvent(
        id: Long,
        type: String,
        homeTeam: String?,
        awayTeam: String?,
        currentScore: String?,
        allRecentScores: String?,
        description: String
    ) =
        if (homeTeam != null && awayTeam != null && currentScore != null && allRecentScores != null) {
            Event.Single(
                homeTeam,
                awayTeam,
                currentScore,
                allRecentScores,
                description,
                Type.valueOf(type)
            )
        } else {
            Event.Multiple(description, Type.valueOf(type))
        }

    internal fun insertEvents(events: List<Event>) {
        dbQuery.transaction {
            events.forEach {
                if (dbQuery.selectEventByDescription(it.description).executeAsOneOrNull() == null) {
                    insertEvent(it)
                }
            }
        }
    }
}