package com.karpenko.karpusha.shared.domain.usecase

import com.karpenko.karpusha.shared.domain.model.CloseableCommonFlow
import com.karpenko.karpusha.shared.domain.model.Event
import com.karpenko.karpusha.shared.domain.model.Result
import com.karpenko.karpusha.shared.domain.model.UseCase

typealias IosIsConnected = Boolean

expect class ObserveEventsUseCase : UseCase<CloseableCommonFlow<Result<List<Event>>>, IosIsConnected>