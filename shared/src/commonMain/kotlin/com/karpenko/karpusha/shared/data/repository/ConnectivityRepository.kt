package com.karpenko.karpusha.shared.data.repository

expect class ConnectivityRepository {
    fun isConnected():Boolean
}