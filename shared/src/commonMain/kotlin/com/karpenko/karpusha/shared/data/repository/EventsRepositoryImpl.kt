package com.karpenko.karpusha.shared.data.repository

import com.karpenko.karpusha.shared.domain.model.CloseableCommonFlow
import com.karpenko.karpusha.shared.domain.model.Event
import com.karpenko.karpusha.shared.domain.model.Result
import com.karpenko.karpusha.shared.domain.model.asCommonFlow
import com.karpenko.karpusha.shared.domain.repository.EventsRepository
import com.karpenko.karpusha.shared.infrastructure.db.Database
import com.karpenko.karpusha.shared.infrastructure.network.EventsApi
import io.ktor.util.*
import kotlinx.coroutines.flow.flow

@KtorExperimentalAPI
class EventsRepositoryImpl(
    private val apiService: EventsApi,
    private val localSource: Database
) : EventsRepository {
    override suspend fun getEvents(isConnected: Boolean): CloseableCommonFlow<Result<List<Event>>> =
        flow {
            emit(Result.Running())
            val cachedEvents = localSource.getAllEvents().toMutableList()
            if (cachedEvents.isNotEmpty()) {
                emit(Result.Success(cachedEvents))
            }
            if (!isConnected) {
                emit(Result.Error("No connection"))
            }
        }.asCommonFlow()

}