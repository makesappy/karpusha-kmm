package com.karpenko.karpusha.shared.domain.model

sealed class Event {
    abstract val description: String
    abstract val type: Type

    data class Single(
        val homeTeam: String,
        val awayTeam: String,
        val currentScore: String,
        val allRecentScores: String,
        override val description: String,
        override val type: Type = Type.OTHER
    ) : Event()

    data class Multiple(
        override val description: String,
        override val type: Type = Type.OTHER
    ) : Event()
}

enum class Type {
    FOOTBALL, TENNIS, HOCKEY, BASKETBALL, OTHER
}