package com.karpenko.karpusha.shared.infrastructure.network

import com.karpenko.karpusha.shared.domain.model.Event
import com.karpenko.karpusha.shared.infrastructure.network.mapper.mapToModel
import io.ktor.client.*
import io.ktor.client.features.websocket.*
import io.ktor.util.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

@KtorExperimentalAPI
class EventsApi {
    companion object {
        private const val EVENTS_WS = "wss://95d6baf17cec.ngrok.io"
    }

    private val httpClient = HttpClient {
        install(WebSockets)
    }

    @Suppress("UnnecessaryVariable", "CanBeVal", "RemoveExplicitTypeArguments")
    suspend fun connectEventsWs(): Flow<List<Event>> {
        var flow = flow<List<Event>> {
            emit(mockJson.mapToModel())
        }
//        TODO connect to sockets when BE will be available
//        httpClient.ws(
//            method = HttpMethod.Get,
//            host = EVENTS_WS
//        ) {
//            flow = incoming.consumeAsFlow()
//                .map { (it as Frame.Text).mapToModel() }
//        }
        return flow
    }
}

const val mockJson = "{\n" +
        "  \"data\": [\n" +
        "    {\n" +
        "      \"homeTeam\": \"Manchester United\",\n" +
        "      \"awayTeam\": \"Blackburn Rovers\",\n" +
        "      \"currentScore\": \"1-3\",\n" +
        "      \"allRecentScores\": \"(0-1)\\n(1-1)\",\n" +
        "      \"description\": \"Фора2 -2,5 : 1.98\",\n" +
        "      \"type\": \"FOOTBALL\"\n" +
        "    },\n" +
        "    {\n" +
        "      \"homeTeam\": \"Мария Шарапова\",\n" +
        "      \"awayTeam\": \"Флина Розенбаум\",\n" +
        "      \"currentScore\": \"5:2\",\n" +
        "      \"allRecentScores\": null,\n" +
        "      \"description\": \"П2\",\n" +
        "      \"type\": \"TENNIS\"\n" +
        "    },\n" +
        "    {\n" +
        "      \"homeTeam\": null,\n" +
        "      \"awayTeam\": null,\n" +
        "      \"currentScore\": null,\n" +
        "      \"allRecentScores\": null,\n" +
        "      \"description\": \"1. Футбол/ Лига Чемпионов УЕФА. Статистика. УГЛ Манчестер Сити - УГЛ Марсель, ИТБ2 (3.5) , 2.80\\n2. Футбол/ Лига Чемпионов УЕФА. Групповой этап. Бавария - Локомотив Москва, ФОРА2 (4), 1.20\\n3. Футбол/ Лига Чемпионов УЕФА. Статистика. УГЛ Зальцбург - УГЛ Атлетико Мадрид, ИТБ2 (5.5), 2.30\\n4. Футбол/ Лига Чемпионов УЕФА. Статистика. Манчестер Сити фолы - Марсель фолы, П2, 2.10\\n5. Футбол/ Лига Чемпионов УЕФА. Статистика. УГЛ Реал Мадрид - УГЛ Боруссия Мёнхенгладбах, ИТБ2 (4.5), 3.10\\nИтого: 50.31\",\n" +
        "      \"type\": null\n" +
        "    },\n" +
        "    {\n" +
        "      \"homeTeam\": \"Manchester United\",\n" +
        "      \"awayTeam\": \"Blackburn Rovers\",\n" +
        "      \"currentScore\": \"1-3\",\n" +
        "      \"allRecentScores\": \"(0-1)\\n(1-1)\",\n" +
        "      \"description\": \"Фора2 -2,5 : 1.98\",\n" +
        "      \"type\": \"FOOTBALL\"\n" +
        "    },\n" +
        "    {\n" +
        "      \"homeTeam\": \"Мария Шарапова\",\n" +
        "      \"awayTeam\": \"Флина Розенбаум\",\n" +
        "      \"currentScore\": \"5:2\",\n" +
        "      \"allRecentScores\": null,\n" +
        "      \"description\": \"П2\",\n" +
        "      \"type\": \"TENNIS\"\n" +
        "    },\n" +
        "    {\n" +
        "      \"homeTeam\": null,\n" +
        "      \"awayTeam\": null,\n" +
        "      \"currentScore\": null,\n" +
        "      \"allRecentScores\": null,\n" +
        "      \"description\": \"1. Футбол/ Лига Чемпионов УЕФА. Статистика. УГЛ Манчестер Сити - УГЛ Марсель, ИТБ2 (3.5) , 2.80\\n2. Футбол/ Лига Чемпионов УЕФА. Групповой этап. Бавария - Локомотив Москва, ФОРА2 (4), 1.20\\n3. Футбол/ Лига Чемпионов УЕФА. Статистика. УГЛ Зальцбург - УГЛ Атлетико Мадрид, ИТБ2 (5.5), 2.30\\n4. Футбол/ Лига Чемпионов УЕФА. Статистика. Манчестер Сити фолы - Марсель фолы, П2, 2.10\\n5. Футбол/ Лига Чемпионов УЕФА. Статистика. УГЛ Реал Мадрид - УГЛ Боруссия Мёнхенгладбах, ИТБ2 (4.5), 3.10\\nИтого: 50.31\",\n" +
        "      \"type\": null\n" +
        "    },\n" +
        "    {\n" +
        "      \"homeTeam\": \"Manchester United\",\n" +
        "      \"awayTeam\": \"Blackburn Rovers\",\n" +
        "      \"currentScore\": \"1-3\",\n" +
        "      \"allRecentScores\": \"(0-1)\\n(1-1)\",\n" +
        "      \"description\": \"Фора2 -2,5 : 1.98\",\n" +
        "      \"type\": \"FOOTBALL\"\n" +
        "    },\n" +
        "    {\n" +
        "      \"homeTeam\": \"Мария Шарапова\",\n" +
        "      \"awayTeam\": \"Флина Розенбаум\",\n" +
        "      \"currentScore\": \"5:2\",\n" +
        "      \"allRecentScores\": null,\n" +
        "      \"description\": \"П2\",\n" +
        "      \"type\": \"TENNIS\"\n" +
        "    },\n" +
        "    {\n" +
        "      \"homeTeam\": null,\n" +
        "      \"awayTeam\": null,\n" +
        "      \"currentScore\": null,\n" +
        "      \"allRecentScores\": null,\n" +
        "      \"description\": \"1. Футбол/ Лига Чемпионов УЕФА. Статистика. УГЛ Манчестер Сити - УГЛ Марсель, ИТБ2 (3.5) , 2.80\\n2. Футбол/ Лига Чемпионов УЕФА. Групповой этап. Бавария - Локомотив Москва, ФОРА2 (4), 1.20\\n3. Футбол/ Лига Чемпионов УЕФА. Статистика. УГЛ Зальцбург - УГЛ Атлетико Мадрид, ИТБ2 (5.5), 2.30\\n4. Футбол/ Лига Чемпионов УЕФА. Статистика. Манчестер Сити фолы - Марсель фолы, П2, 2.10\\n5. Футбол/ Лига Чемпионов УЕФА. Статистика. УГЛ Реал Мадрид - УГЛ Боруссия Мёнхенгладбах, ИТБ2 (4.5), 3.10\\nИтого: 50.31\",\n" +
        "      \"type\": null\n" +
        "    },\n" +
        "    {\n" +
        "      \"homeTeam\": \"Мария Шарапова\",\n" +
        "      \"awayTeam\": \"Флина Розенбаум\",\n" +
        "      \"currentScore\": \"5:2\",\n" +
        "      \"allRecentScores\": null,\n" +
        "      \"description\": \"П2\",\n" +
        "      \"type\": \"TENNIS\"\n" +
        "    }\n" +
        "  ]\n" +
        "}"