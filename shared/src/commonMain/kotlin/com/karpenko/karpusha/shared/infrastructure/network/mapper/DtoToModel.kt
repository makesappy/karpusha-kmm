package com.karpenko.karpusha.shared.infrastructure.network.mapper

import com.karpenko.karpusha.shared.domain.model.Event
import com.karpenko.karpusha.shared.domain.model.Type
import com.karpenko.karpusha.shared.infrastructure.network.model.EventDto
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

fun String.mapToModel() =
    Json.decodeFromString<List<EventDto>>(this).map {
        with(it) {
            if (homeTeam != null && awayTeam != null && currentScore != null && allRecentScores != null) {
                Event.Single(
                    homeTeam,
                    awayTeam,
                    currentScore,
                    allRecentScores,
                    description,
                    type?.let { _type ->
                        Type.valueOf(_type)
                    } ?: Type.OTHER
                )
            } else {
                Event.Multiple(description, Type.OTHER)
            }
        }
    }