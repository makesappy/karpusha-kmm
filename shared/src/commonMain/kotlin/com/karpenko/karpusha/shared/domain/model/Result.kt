package com.karpenko.karpusha.shared.domain.model


sealed class Result<out T : Any> {

    data class Running<out T : Any>(val data: T? = null) : Result<T>()

    data class Success<out T : Any>(val data: T) : Result<T>()

    data class Error<out T : Any>(val error: String) : Result<T>()

    override fun toString(): String {
        return when (this) {
            is Success -> "Success[data=$data]"
            is Error -> "Error[exception=${error}"
            is Running -> "Running[cachedData=$data]"
        }
    }

    fun isFinished() = this is Success || this is Error

    fun isRunning() = this is Running

    fun isSuccess() = this is Success

    fun isError() = this is Error

    /**
     * Returns the encapsulated value if this instance represents [Success] or cached data when available in [Running] state
     */
    fun getOrNull() = when {
        this is Success -> data
        this is Running -> data
        this is Error -> null
        else -> null
    }

    /**
     * Returns the encapsulated error if this instance represents [Error]
     */
    fun errorOrNull() = when {
        this is Error -> error
        else -> null
    }

    /**
     * Returns [Result] of same type ([Result.Success], [Result.Error] or [Result.Running]) but with different "data" type.
     * Original data are transformed by passed [dataTransform] function.
     *
     * **Example**
     * ```
     * suspend fun getActiveVehicleCode():Result<VehicleCode> =
     *     getActiveVehicleUseCase().map { vehicle ->
     *         vehicle.code
     *     }
     * ```
     * @param dataTransform Function that transforms data from one type to another
     * @param R Target "data" type
     *
     * @return Result of same type but with different data type.
     */
    inline fun <R : Any> map(dataTransform: (T) -> R): Result<R> = when (this) {
        is Success -> Success(dataTransform(data))
        is Error -> Error(error)
        is Running -> Running(data?.let { dataTransform(it) })
    }

}

open class ErrorResult(open var message: String? = null, open var throwable: Throwable? = null)

/**
 * Calls the specified function [block] with `this` value as its receiver and returns its encapsulated result if invocation was successful,
 * catching any [Throwable] exception that was thrown from the [block] function execution and encapsulating it as a failure.
 */
inline fun <T, R : Any> T.runCatching(block: T.() -> R): Result<R> {
    return try {
        Result.Success(block())
    } catch (e: Throwable) {
        Result.Error("")
    }
}