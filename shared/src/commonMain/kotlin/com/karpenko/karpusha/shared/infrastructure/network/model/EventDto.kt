package com.karpenko.karpusha.shared.infrastructure.network.model

import kotlinx.serialization.Serializable

@Serializable
data class EventDto(
       val homeTeam: String?,
       val awayTeam: String?,
       val currentScore: String?,
       val allRecentScores: String?,
       val description: String,
       val type: String?
)