buildscript {
    repositories {
        gradlePluginPortal()
        jcenter()
        google()
        mavenCentral()
        maven(URL.snapshots)
        maven(URL.koinEkito)
    }

    dependencies {
        classpath(Deps.kotlinBuild)
        classpath(Deps.buildGradle)
        classpath(Deps.serialization)
        classpath(Deps.SqlDelight.gradle)
        classpath(Deps.Koin.gradle)
    }
}
group = Sdk.sdkGroup
version = Sdk.sdkVersion

repositories {
    mavenCentral()
    maven(URL.snapshots)
    maven(URL.koinEkito)
}
