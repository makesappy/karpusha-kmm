object URL {
    const val snapshots = "https://oss.sonatype.org/content/repositories/snapshots"
    const val koinEkito = "https://dl.bintray.com/ekito/koin"
}
object Sdk {
    const val sdkGroup = "com.karpenko.karpusha"
    const val sdkVersion = "0.0.1-SNAPSHOT"
}