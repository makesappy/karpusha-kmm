object Versions {
    const val kotlin = "1.4.10"
    const val koin = "3.0.0-alpha-4"
    const val lifecycle = "2.2.0-alpha01"
    const val compose = "1.0.0-alpha06"
    const val junit = "4.13"
    const val coroutines = "1.3.9-native-mt"
    const val sqlDelight = "1.5.0-SNAPSHOT"
    const val navComposeVersion = "1.0.0-alpha01"
    const val serializationVersion = "1.0.0-RC"
    const val ktorVersion = "1.4.2"
    const val material = "1.3.0-alpha03"
    const val buildGradle = "4.0.1"
    const val appCompat = "1.2.0"
    const val constraint = "2.0.4"
    const val ktx = "1.3.2"
    const val recycler = "1.1.0"
    const val cardView = "1.0.0"
}

object Deps {

    const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}"
    const val material = "com.google.android.material:material:${Versions.material}"
    const val junit = "junit:junit:${Versions.junit}"
    const val serialization = "org.jetbrains.kotlin:kotlin-serialization:${Versions.kotlin}"
    const val buildGradle = "com.android.tools.build:gradle:${Versions.buildGradle}"
    const val kotlinBuild = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    const val serializationCore = "org.jetbrains.kotlinx:kotlinx-serialization-core:${Versions.serializationVersion}"


    object KTor {
        const val core = "io.ktor:ktor-client-core:$${Versions.ktorVersion}"
        const val serialization = "io.ktor:ktor-client-serialization:${Versions.ktorVersion}"
        const val android = "io.ktor:ktor-client-android:${Versions.ktorVersion}"
        const val ios = "io.ktor:ktor-client-ios:${Versions.ktorVersion}"
    }

    object Koin {
        const val compose = "org.koin:koin-androidx-compose:${Versions.koin}"
        const val core = "org.koin:koin-core:${Versions.koin}"
        const val androidViewModel = "org.koin:koin-androidx-viewmodel:${Versions.koin}"
        const val test = "org.koin:koin-test:${Versions.koin}"
        const val gradle = "org.koin:koin-gradle-plugin:${Versions.koin}"
    }

    object Android {
        const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
        const val constraint = "androidx.constraintlayout:constraintlayout:${Versions.constraint}"
        const val coreKtx = "androidx.core:core-ktx:${Versions.ktx}"
        const val recycler = "androidx.recyclerview:recyclerview:${Versions.recycler}"
        const val cardView = "androidx.cardview:cardview:${Versions.cardView}"
        const val viewModelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
    }

    object KotlinTest {
        const val common = "org.jetbrains.kotlin:kotlin-test-common:${Versions.kotlin}"
        const val annotations =
            "org.jetbrains.kotlin:kotlin-test-annotations-common:${Versions.kotlin}"
        const val jvm = "org.jetbrains.kotlin:kotlin-test:${Versions.kotlin}"
        const val junit = "org.jetbrains.kotlin:kotlin-test-junit:${Versions.kotlin}"
    }

    object Coroutines {
        const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"
        const val android =
            "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
        const val test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutines}"
    }

    object SqlDelight {
        const val gradle = "com.squareup.sqldelight:gradle-plugin:${Versions.sqlDelight}"
        const val runtime = "com.squareup.sqldelight:runtime:${Versions.sqlDelight}"
        const val runtimeJdk = "com.squareup.sqldelight:runtime-jvm:${Versions.sqlDelight}"
        const val driverIos = "com.squareup.sqldelight:native-driver:${Versions.sqlDelight}"
        const val driverAndroid = "com.squareup.sqldelight:android-driver:${Versions.sqlDelight}"
        const val runtimeMacOs = "com.squareup.sqldelight:runtime-macosx64:${Versions.sqlDelight}"
    }
}

