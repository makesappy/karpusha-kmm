import SwiftUI
import shared

struct ContentView: View {
  @ObservedObject private(set) var viewModel: ViewModel

    var body: some View {
        NavigationView {
            listView()
            .navigationBarTitle("Events")
            .navigationBarItems(trailing:
                Button("Reload") {
                    self.viewModel.loadEvents(forceReload: true)
            })
        }
    }

    private func listView() -> AnyView {
        switch viewModel.events {
        case .loading:
            return AnyView(Text("Loading...").multilineTextAlignment(.center))
        case .result(let events):
            return AnyView(List(events) { event in
                EventRow(event: event)
            })
        case .error(let description):
            return AnyView(Text(description).multilineTextAlignment(.center))
        }
    }
}

extension ContentView {
    
    enum LoadableEvents {
        case loading
        case result([Event])
        case error(String)
    }
    
    class ViewModel: ObservableObject {
        let observeUseCase: ObserveEventsUseCase
        @Published var events = LoadableEvents.loading
        
        init(observeUseCase: ObserveEventsUseCase) {
            self.observeUseCase = observeUseCase
            self.loadEvents(forceReload: false)
        }
        
        func loadEvents(forceReload: Bool) {
            self.events = .loading
            observeUseCase.invoke(true)(completionHandler: { eventsFlow, errorFlow in
                eventsFlow?.watch{events in
                    if let events = events {
                        self.events = .result(events as! [Event])
                    } else {
                        self.events = .error(errorFlow?.localizedDescription ?? "error")
                    }
                }
            })
        }
    }
}

extension Event: Identifiable { }
