//
//  EventRow.swift
//  iosApp
//
//  Created by Heorhii  Karpenko on 31.10.2020.
//  Copyright © 2020 orgName. All rights reserved.
//

import SwiftUI
import shared

struct EventRow: View {
    var event: Event

    var body: some View {
        HStack() {
            VStack(alignment: .leading, spacing: 10.0) {
                Text("Launch name: \(event.data)")
            }
            Spacer()
        }
    }
}

struct EventRow_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
